<?php 

class CFB_Plugin_Settings {

	private $base;

	function __construct($base = ''){
		$this->base = $base;
	}

	public function __get($name) {
		
		return get_option('cfb-settings-'.$this->get_base_slug().$name);
	}

	public function __set($name, $value) {

		if (!is_array($value)){
			$value = trim($value);
		}

		$new_name = 'cfb-settings-'.$this->get_base_slug().$name;
		$is_updated = update_option($new_name, $value);
		if (!$is_updated){
			error_log("Setting {$name} not updated");
		}
	}

	private function get_base_slug(){
		$default = 'cfb-settings-';
		if ($this->base) $default .= $this->base.'-';

		return $default;
	}

}
