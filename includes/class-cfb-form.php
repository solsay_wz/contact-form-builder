<?php 

class CFB_Form {
	
	private $form_id;
//
//	private $form_builder_data;
//    private $html;
//	private $from;
//	private $to;
//	private $subject;
//	private $additional_headers;
//	private $body;
//	private $messages;
//	private $form_css;

	private $form_data;

	function __construct($id, $form_data = []) {
		$this->form_id = $id;
		$this->form_data = $form_data;
	}

	public function validate_form(){
		$fields_settings = $this->get_form_builder_data();
		if (!$fields_settings) return;
		$fields_settings = json_decode($fields_settings, true);
		$messages = $this->get_messages();
		$error_fields = [];

		foreach ($fields_settings as $field){
			$required = isset( $field['required'] ) ? $field['required'] : false;
			$name = isset( $field['name'] ) ? $field['name'] : false;
			$type = isset( $field['type'] ) ? $field['type'] : false;
			$subtype = isset( $field['subtype'] ) ? $field['subtype'] : false;
			$val = isset( $this->form_data[ $name ] ) ? $this->form_data[ $name ] : false;

			if ($required){
				if ($type === 'file'){
					$file_arr = $val;
					if (!$file_arr){
						$error_fields[ $name ] = $messages['unexpect_file'];
					}elseif (empty($file_arr['tmp_name'])){
						$error_fields[ $name ] = $messages['invalid_required'];
					}
                }elseif ( $type === 'checkbox-group' && isset($field['values']) ){
                    $must_have = array_map(function ($v){
                        return $v['value'];
                    }, $field['values']);

                    sort($must_have);
                    sort($val);

                    if ($must_have != $val){
                        $error_fields[ $name . '[]' ] = $messages['invalid_required'];
                    }
				}elseif ( empty($val) ){
					$error_fields[ $name ] = $messages['invalid_required'];
				}
			}


            // Format val
			if ($val){
				if ($subtype === 'email'){

				    if ( strpos($val, '@') === false || strpos($val, '.') === false ){
                        $error_fields[ $name ] = $messages['invalid_mail_symbol'];

                    }elseif (!filter_var($this->form_data[ $name ], FILTER_VALIDATE_EMAIL)) {
                        $error_fields[ $name ] = $messages['invalid_mail'];
					}

				}

				if ($subtype === 'tel'){
					$nums = preg_replace("/[^0-9]/", '', $this->form_data[ $name ]);
					if (strlen($nums) < 8){
						$error_fields[ $name ] = $messages['invalid_tel'];
					}
				}
			}
			

		}

		$error_fields = apply_filters('cfb_validate_form', $error_fields, $this->form_data, $this->form_id);
		return $error_fields;
	}

	public function submit(){
		if (! $this->form_data ){
			return new WP_Error( 'cfb_unexpected',
				__( 'Empty form', CFB_TEXT_DOMAIN )
			);
		}

		$form_vars = [
			'to' => $this->get_to(),
			'from' => $this->get_from(),
			'subject' => $this->get_subject(),
			'additional_headers' => $this->get_additional_headers(),
			'body' => $this->get_body()
		];

		$files = [];
		foreach ($this->form_data as $name => $val){

			$value_to_replace = false;

			// If this file
			if ( isset($val['tmp_name']) && isset($val['size']) ){
				$value_to_replace = $val['name'].' '.__('( In attachments )', CFB_TEXT_DOMAIN);
				$files[] = $val['tmp_name'];

			// If checkbox or multiselect
			}elseif (is_array($val)){
				$value_to_replace = implode(', ', $val);

			// If text\num\single
			}else{
				$value_to_replace = $val;
			}


			// Replace shortcodes
			$shortcode = '['.$name.']';

			foreach ($form_vars as $var_name => $var) {
				$form_vars[$var_name] = str_replace($shortcode, $value_to_replace, $var);
			}

		}

		// Strip non parsed sortcodes
		foreach ($form_vars as $var_name => $var) {
			$form_vars[$var_name] = preg_replace('/\[.*?\]/', '', $var);
		}

		$headers = "From: {$form_vars['from']}\n";
		$headers .= "Content-Type: text/html\n";
		$headers .= "X-CFB-Content-Type: text/html\n";

		if ( $form_vars['additional_headers'] ) {
			$headers .= $form_vars['additional_headers'] . "\n";
		}

		do_action('cfb_before_send_mail', $this->form_data, $this);

		return wp_mail( $form_vars['to'], $form_vars['subject'], $form_vars['body'], $headers, $files );
	}

	public function get_form_builder_data(){
		return get_post_meta($this->form_id, 'form_builder_data', true);
	}

	public function set_form_builder_data($form_builder_data){

		return $this->update_setting('form_builder_data', $form_builder_data);
	}

    public function get_html(): string {
        return get_post_meta($this->form_id, 'html', true);
    }

	public function set_html($html){
	    $html = apply_filters('cfb_before_save_html', $html, $this->get_id());

		return $this->update_setting('html', $html);
	}

	public function get_from(){
		$saved_from = get_post_meta($this->form_id, 'from', true);

		if (!$saved_from){
			$curent_user = wp_get_current_user();
			$nickname = $curent_user->nickname;
			$site_title = get_bloginfo('name');

			$saved_from = $site_title.' <'.$nickname.'@'.$_SERVER['SERVER_NAME'].'>';
		}

		return $saved_from;
	}

	public function set_from($from){
		return $this->update_setting('from', $from);
	}

	public function get_to(){
		$saved_to = get_post_meta($this->form_id, 'to', true);

		if (!$saved_to){
			$general_settings = new CFB_Plugin_Settings();
			$saved_to = isset($general_settings->general['default_to']) ? $general_settings->general['default_to'] : '';
		}

		return $saved_to;
	}

	public function set_to($to){
		return $this->update_setting('to', $to);
	}

	public function get_subject(){
		return get_post_meta($this->form_id, 'subject', true);
	}

	public function set_subject($subject){
		return $this->update_setting('subject', $subject);
	}

	public function get_additional_headers(){
		return get_post_meta($this->form_id, 'additional_headers', true);
	}

	public function set_additional_headers($additional_headers){
		return $this->update_setting('additional_headers', $additional_headers);
	}

	public function get_body(){
		return get_post_meta($this->form_id, 'body', true);
	}

	public function set_body($body){
		return $this->update_setting('body', $body);
	}

	public function get_messages(){
		return get_post_meta($this->form_id, 'messages', true);
	}

	public function set_messages($messages){
		return $this->update_setting('messages', $messages);
	}

	public function get_form_css(){
		return get_post_meta($this->form_id, 'form_css', true);
	}

	public function set_form_css($form_css){
		return $this->update_setting('form_css', $form_css);
	}

	public function get_id(){
		return $this->form_id;
	}


	private function update_setting($name, $value){
		if (!is_array($value)){
			$value = trim($value);
		}
		
		$is_updated = update_post_meta($this->form_id, $name, $value);
		return $is_updated;
	}

}