<?php 

class CFB_Post_Type {

	public static function register_post_type(){
		register_post_type('cfb-form', [
			'labels'			=> [
			    'name'					=> __( 'Forms', CFB_TEXT_DOMAIN),
				'singular_name'			=> __( 'Form', CFB_TEXT_DOMAIN ),
			    'add_new'				=> __( 'Add New' , CFB_TEXT_DOMAIN ),
			    'add_new_item'			=> __( 'Add New Form' , CFB_TEXT_DOMAIN ),
			    'edit_item'				=> __( 'Edit Form' , CFB_TEXT_DOMAIN ),
			    'new_item'				=> __( 'New Form' , CFB_TEXT_DOMAIN ),
			    'view_item'				=> __( 'View Form', CFB_TEXT_DOMAIN ),
			    'search_items'			=> __( 'Search Forms', CFB_TEXT_DOMAIN ),
			    'not_found'				=> __( 'No Forms found', CFB_TEXT_DOMAIN ),
			    'not_found_in_trash'	=> __( 'No Forms found in Trash', CFB_TEXT_DOMAIN ), 
			],
			'public'			  => true,
			'hierarchical'		  => true,
			'show_ui'			  => true,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'capability_type'	  => 'post',
			'supports' 			  => ['title'],
			'menu_icon' 		  => 'dashicons-email-alt'
			
		]);
	}
}