<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wizardsdev.com/
 * @since             1.0.0
 * @package           Contact_Form_Builder
 *
 * Plugin Name:       Contact Form Builder
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            WizardsDev
 * Author URI:        https://wizardsdev.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       contact-form-builder
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class CFB {
	
	/** @var string The plugin version number. */
	var $version = '1.0.0';
	
	/**
	 * initialize
	 *
	 */
	function initialize() {
		
		// Define constants.
		$this->define( 'CFB_PATH', plugin_dir_path( __FILE__ ) );
		$this->define( 'CFB_TEXT_DOMAIN', plugin_dir_path( __FILE__ ) );
		$this->define( 'CFB_BASENAME', plugin_basename( __FILE__ ) );
		$this->define( 'CFB_PLUGIN_URI', plugin_dir_url( __FILE__ ) );
		$this->define( 'CFB_VERSION', $this->version );
		
		// Primari classes
		include_once( CFB_PATH . 'includes/class-cfb-post-type.php');
		include_once( CFB_PATH . 'includes/class-cfb-form.php');
		include_once( CFB_PATH . 'includes/class-cfb-plugin-settings.php');

		// Basic Integrations
		include_once( CFB_PATH . 'integrations/abstract-cfb-integration.php');
		include_once( CFB_PATH . 'integrations/class-cfb-integrations-service.php');

		// Integrations Depends
		include_once( CFB_PATH . 'integrations/libs/vendor/autoload.php');

		// Integrations
		include_once( CFB_PATH . 'integrations/google-sheets/google-sheets.php');
		include_once( CFB_PATH . 'integrations/recaptcha_v3/recaptcha.php');
		include_once( CFB_PATH . 'integrations/zoho-lead/zoho-lead.php');
		
		// Include admin.
		if( is_admin() ) {
			include_once( CFB_PATH . 'admin/class-contact-form-builder-admin.php');
			include_once( CFB_PATH . 'admin/class-contact-form-metabox.php');

			new Contact_Form_Builder_Admin();

		// Include front-end
		}else{
            include_once( CFB_PATH . 'public/class-cfb-rest-api.php');
            include_once( CFB_PATH . 'public/class-cfb-frontend.php');
			CFB_FrontEnd::init();
		}

		// Add Integrations
		$integrations = CFB_Integrations_Service::get_instance();
		$integrations->add_integration(new Recapcha());
		$integrations->add_integration(new Google_Sheets());
		$integrations->add_integration(new Zoho_Lead());
		

		// Hooks
		add_action( 'init', [CFB_Post_Type::class, 'register_post_type'], 5 );
	}


	/**
	 * Defines a constant if doesnt already exist.
	 * @return	void
	 */
	private function define( $name, $value = true ) {
		if( !defined($name) ) {
			define( $name, $value );
		}
	}

}

function run(){
	$plugin = new CFB();
	$plugin->initialize();
}

run();


