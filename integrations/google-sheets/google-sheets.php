<?php

class Google_Sheets extends CFB_Integration{

    const clientId = '292845406308-j9s416fekqen6kest0ksgchf3bl9l54q.apps.googleusercontent.com';
    const clientSecret = '6XR1gCuK7iu7AezFvFggbUQF';
    const redirect = 'urn:ietf:wg:oauth:2.0:oob';

	function __construct() {
		$this->settings = new CFB_Plugin_Settings( $this->get_slug() );
		if ($this->settings->enable){
			add_action('cfb_before_send_mail', [$this, 'save_to_sheet'], 10, 2);
		}
	}

	
	public function get_name(){
		return __('Google Sheets', CFB_TEXT_DOMAIN);
	}

	public function get_slug(){
		return 'google-sheet';
	}

	public function get_settings_fields(){
		$fields = [
			[
				'name' => 'enable',
				'label' => __('Enable?', CFB_TEXT_DOMAIN),
				'type' => 'checkbox',
				'value' => 'on'
			],
			[
				'name' => 'sheet_id',
				'label' => __('Spreadsheet ID', CFB_TEXT_DOMAIN),
				'type' => 'text',
                'class' => 'regular-text',
            ],
            [
                'name' => 'gs_code',
                'class' => 'regular-text',
                'label' => __('Google Sheet auth code', CFB_TEXT_DOMAIN),
                'type' => 'text',
                'additional_link_html' => '<a href="https://accounts.google.com/o/oauth2/auth?access_type=offline&approval_prompt=force&client_id=292845406308-j9s416fekqen6kest0ksgchf3bl9l54q.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&response_type=code&scope=https%3A%2F%2Fspreadsheets.google.com%2Ffeeds%2F+https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/drive.metadata.readonly" target="_blank" class="button">Get Code</a>',
            ],

        ];

		return $fields;
	}

	public function get_client(){
        $tokenData = json_decode($this->settings->gs_token, true);

        if ( !isset( $tokenData['refresh_token'] ) || empty( $tokenData['refresh_token'] ) ) {
            throw new LogicException( "Auth, Invalid OAuth2 access token" );
            exit();
        }

        try {
            $client = new Google_Client();
            $client->setClientId( Google_Sheets::clientId );
            $client->setClientSecret( Google_Sheets::clientSecret );
            $client->setScopes( Google_Service_Sheets::SPREADSHEETS );
            $client->setScopes( Google_Service_Drive::DRIVE_METADATA_READONLY );
            $client->refreshToken( $tokenData['refresh_token'] );
            $client->setAccessType( 'offline' );
            $this->updateToken( $tokenData );

            return $client;
        } catch ( Exception $e ) {
            $this->log( "Auth, Error fetching OAuth2 access token, message: " . $e->getMessage() );
        }
    }

    public function verify_gs_integation($code) {
        $client = new Google_Client();
        $client->setClientId( Google_Sheets::clientId );
        $client->setClientSecret( Google_Sheets::clientSecret );
        $client->setRedirectUri( Google_Sheets::redirect );
        $client->setScopes((array)Google_Service_Sheets::SPREADSHEETS);
        $client->setScopes((array)Google_Service_Drive::DRIVE_METADATA_READONLY);
        $client->setAccessType( 'offline' );
        $client->fetchAccessTokenWithAuthCode( $code );
        $tokenData = $client->getAccessToken();
        $this->updateToken( $tokenData );
    }

    public function updateToken( $tokenData ) {
	    if (!$tokenData) return;
        $tokenData['expire'] = time() + intval( $tokenData['expires_in'] );
        try {
            $tokenJson = json_encode( $tokenData );
            $this->settings->gs_token = $tokenJson;
        } catch ( Exception $e ) {
            $this->log( "Token write fail! - " . $e->getMessage() );
        }
    }

	public function save_settings_form(){
		if (!$_POST) return;
		check_admin_referer( 'cfb-'.$this->get_slug().'-setup' );

        $gs_code = !empty($_POST['gs_code']) ? $_POST['gs_code'] : false;
        $sheet_id = !empty($_POST['sheet_id']) ? $_POST['sheet_id'] : false;
		$enable = !empty($_POST['enable']) ? $_POST['enable'] : false;

		$this->settings->enable = $enable;
        $this->settings->sheet_id = $sheet_id;

        if ($gs_code){
            $this->verify_gs_integation($gs_code);
        }

//        if ( isset($_FILES['file_credentials']) &&
//			!empty($_FILES['file_credentials']['tmp_name']) &&
//			$_FILES['file_credentials']['type'] === 'application/json'){
//			move_uploaded_file($_FILES['file_credentials']['tmp_name'], __DIR__.'/credentials.json');
//
//			$this->settings->file_credentials = $_FILES['file_credentials']['name'];
//		}

	}

	public function save_to_sheet($form_data, $form){
		$sheet_id = $this->get_sheet_id();
		$credentials_file = $this->get_credentials_file();

		if (!$sheet_id || !$credentials_file) return false;

		$client = $this->get_client();
		$service = new Google_Service_Sheets($client);

		$form_id = $form->get_id();
		$this->create_sheet($service, get_the_title($form_id));


		$fields_settings = $form->get_form_builder_data();
		if (!$fields_settings) return;
		$fields_settings = json_decode($fields_settings, true);

		foreach ($form_data as $field_name => $val){

			$is_find = false;
			foreach ($fields_settings as $field){
				$name = isset( $field['name'] ) ? $field['name'] : false;
				$type = isset( $field['type'] ) ? $field['type'] : false;

				if ($field_name === $name){
					if ($type === 'file'){
						$form_data[$field_name] = $val['tmp_name'];
					}else{

						$form_data[$field_name] = (is_array($val)) ? implode(', ', $val) : $val;
					}

					$is_find = true;
					break;
				}
			}

			if (!$is_find) unset($form_data[$field_name]);
		}

		$values = [
			array_values($form_data)
		];
		$body = new Google_Service_Sheets_ValueRange([
			'values' => $values,
			'majorDimension' => 'ROWS'
		]);
		$params = [
			'valueInputOption' => 'USER_ENTERED',
            'insertDataOption' => 'INSERT_ROWS'
		];

		//Append
		$range = get_the_title($form_id).'!A1';

		try{
			$result = $service->spreadsheets_values->append($this->get_sheet_id(), $range,
				$body, $params);
			var_dump($result);
			return $result;
		}catch(Exception $e){
			$this->log(var_export($e, true));
		}
		
	}

	private function create_sheet($service, $name){
		try {
			$body = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
				'requests' => array(
					'addSheet' => array(
						'properties' => array(
							'title' => $name
						)
					)
				)
			));
			$result = $service->spreadsheets->batchUpdate($this->get_sheet_id(),$body);
			return $result;
		} catch(Exception $ignore) {
		}

	}

	private function get_sheet_id(){
		return $this->settings->sheet_id;
	}

	private function get_credentials_file(){
		$credentials_dir = __DIR__ . '/credentials.json';

		return file_exists( $credentials_dir ) ? $credentials_dir : false;
	}


}