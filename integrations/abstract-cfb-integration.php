<?php 

abstract class CFB_Integration{

	protected $settings;

	public function __construct(){
		$this->settings = new CFB_Plugin_Settings( $this->get_slug() );
	}

	abstract public function get_name();
	abstract public function get_slug();

	public function get_settings_fields(){
		return [];
	}

	public function save_settings_form(){
		if (!$_POST) return;
	}

	public function render_page(){
		include_once( CFB_PATH . 'admin/partials/settings-form.php');
	}

	public function get_settings(){
		return $this->settings;
	}

	protected function log($string, $form_id = false){
		$upload_dir = wp_get_upload_dir();
		$base_upload_dir = $upload_dir['basedir'];
		
		$base_log_dir = $base_upload_dir.'/cfb/';
		if ( !file_exists( $base_log_dir ) && !is_dir( $base_log_dir ) ) {
			mkdir( $base_log_dir );       
		} 

		$integration_log_dir = $base_log_dir.$this->get_slug();
		if ( !file_exists( $integration_log_dir ) && !is_dir( $integration_log_dir ) ) {
			mkdir( $integration_log_dir );       
		} 


		if ( $form_id ){
			$integration_log_dir .= '/'.$form_id.'/';
			if ( !file_exists( $integration_log_dir ) && !is_dir( $integration_log_dir ) ) {
				mkdir( $integration_log_dir );       
			} 
		}

		$integration_log_file = $integration_log_dir.date('d-m-Y').'.log';

		// Write
		$mod_string = '<b>[ '.$this->get_name().' ]</b> '.date('H:i:s').PHP_EOL.'<br>';
		$mod_string .= $string.PHP_EOL.PHP_EOL.'<br>'.'<br>';
		file_put_contents($integration_log_file, $mod_string, FILE_APPEND);
	}

}