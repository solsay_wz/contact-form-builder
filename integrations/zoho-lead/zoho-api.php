<?php

use zcrmsdk\crm\setup\restclient\ZCRMRestClient;
use zcrmsdk\oauth\ZohoOAuth;

/**
 * Simple Zoho CRM inserter.
 *
 * MIT licensed. Originally written by Pete Sevander and Mikko Ohtamaa in 2011
 * Enhanced by Jeremy Nagel to make it a bit easier to use and update it with latest changes to the API
 *
 */

class Zoho_API {

    private $apiUrl = 'https://www.zohoapis.com/crm/v2/';
    private $headers = [];

    public function __construct($settings) {

        $grant_token = $settings->auth_grant_token;
        $client_id = $settings->client_id;
        $client_secret = $settings->client_secret;
        $redirect_uri = $settings->redirect_uri;
        $current_user_email = $settings->current_user_email;

        $config = [
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'redirect_uri' => $redirect_uri,
            'currentUserEmail' => $current_user_email,
            'token_persistence_path' => ABSPATH.'wp-content/oauthtokens/',
            'applicationLogFilePath' => ABSPATH.'wp-content/oauthtokens/logs/'
        ];
        foreach ($config as $val){
            if (!$val) return;
        }

        ZCRMRestClient::initialize($config);
        $oAuthClient = ZohoOAuth::getClientInstance();
        $authtoken = $oAuthClient->getAccessToken($settings->auth_email);

        //$oAuthClient->generateAccessToken($grantToken);
        // $authtoken = $oAuthClient->getAccessToken($this->configuration['currentUserEmail']);

        $this->headers = ['Authorization: Zoho-oauthtoken '.$authtoken, 'Content-Type: application/json'];
    }

    public function insert_record($module = 'Leads', $update_data, $extra_post_parameters=[]) {

        $data = ['data' => $update_data];
        array_merge($data, $extra_post_parameters);
        $response = $this->openUrl($this->apiUrl.$module, $data);

        if($response['data'][0]['code'] == 'DUPLICATE_DATA'){
            $this->openUrl($this->apiUrl.$module.'/'.$response['data'][0]['details']['id'], $data, 'PUT');
            return 'dublicate_data';
        }

        $response = $response;

        if ($response['data'][0]['status'] == 'error') {
            return $response['data'][0]['message'];
        }

        return 'data_sent';
    }

    /**
     * Uses curl to open a URL
     * @param $url
     * @param null $data
     * @param string $method
     * @return bool|string|null
     */
    function openUrl($url, $data=null, $method='POST') {

        $ch = curl_init();

        if($data) {
            curl_setopt($ch,CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        }

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch,CURLOPT_MAXREDIRS,10);
        curl_setopt($ch,CURLOPT_TIMEOUT,30);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $this->headers);

        $data = json_decode(curl_exec($ch), true);
        curl_close($ch);
        return $data;
    }

}
