<?php 

class Zoho_Lead extends CFB_Integration{

	function __construct() {
		$this->settings = new CFB_Plugin_Settings( $this->get_slug() );

		if (!empty($this->settings->enable)){
			require_once __DIR__.'/zoho-api.php';

			add_filter('cfb_custom_fields_atts', [$this, 'custom_fields_atts']);
			add_action('cfb_before_send_mail', [$this, 'save_to_zoho'], 10, 2);
		}

	}

	
	public function get_name(){
		return __('Zoho Lead', CFB_TEXT_DOMAIN);
	}

	public function get_slug(){
		return 'zoho_lead';
	}

	public function custom_fields_atts($fileds){
		$zoho_field_type = [
			'label' => __('Zoho Field Type'),
			'options' => [
				'company' => 'Company',
				'first-name' => 'First_Name',
				'last-name' => 'Last_Name',
				'email' => 'Email',
				'phone' => 'Phone',
				'title' => 'Title'
			]

		];
		$fileds['text'] = [
			'zoho_type' => $zoho_field_type
		];

		return $fileds;
	}

	public function save_to_zoho($form_fields, $form){
		$fields_settings = $form->get_form_builder_data();
		if (!$fields_settings) return;
		$fields_settings = json_decode($fields_settings, true);

		$lead = [];

		foreach ($fields_settings as $field){
			$required = isset( $field['required'] ) ? $field['required'] : false;
			$name = isset( $field['name'] ) ? $field['name'] : false;
			$type = isset( $field['type'] ) ? $field['type'] : false;
			$subtype = isset( $field['subtype'] ) ? $field['subtype'] : false;
			$zoho_type = isset( $field['zoho_type'] ) ? $field['zoho_type'] : false;
			$val = isset( $form_fields[ $name ] ) ? $form_fields[ $name ] : false;

			if ($zoho_type === 'company'){
				$lead['Company'] = $val;
			}elseif($zoho_type === 'first-name'){
				$lead['First_Name'] = $val;
			}elseif($zoho_type === 'last-name'){
				$lead['Last_Name'] = $val;
			}elseif($zoho_type === 'email'){
				$lead['Email'] = $val;
			}elseif($zoho_type === 'phone'){
				$lead['Phone'] = $val;
			}elseif($zoho_type === 'title'){
				$lead['Title'] = $val;
			}
		}

		$zoho_api = new Zoho_API($this->settings);

		$response = $zoho_api->insert_record('Leads', (array($lead)));
		log('Zoho API response: %s', print_r($response, true));
	}



	public function get_settings_fields(){
		$fields = [
			[
				'name' => 'enable',
				'label' => __('Enable?', CFB_TEXT_DOMAIN),
				'type' => 'checkbox',
				'value' => 'on'
			],
			[
				'name' => 'client_id',
				'label' => __('Client ID', CFB_TEXT_DOMAIN),
				'type' => 'text',
				'class' => 'regular-text',
			],
			[
				'name' => 'client_secret',
				'label' => __('Client Secret', CFB_TEXT_DOMAIN),
				'type' => 'text',
				'class' => 'regular-text'
			],
			[
				'name' => 'redirect_uri',
				'label' => __('Redirect URI', CFB_TEXT_DOMAIN),
				'type' => 'text',
				'class' => 'regular-text'
			],
			[
				'name' => 'current_user_email',
				'label' => __('Current User Email', CFB_TEXT_DOMAIN),
				'type' => 'email'
			]
		];

		return $fields;
	}

	public function save_settings_form(){
		if (!$_POST) return;
		check_admin_referer( 'cfb-'.$this->get_slug().'-setup' );

		$client_id = !empty($_POST['client_id']) ? $_POST['client_id'] : false;
		$client_secret = !empty($_POST['client_secret']) ? $_POST['client_secret'] : false;
		$redirect_uri = !empty($_POST['redirect_uri']) ? $_POST['redirect_uri'] : false;
		$current_user_email = !empty($_POST['current_user_email']) ? $_POST['current_user_email'] : false;
		$enable = !empty($_POST['enable']) ? $_POST['enable'] : false;

		$this->settings->client_id = $client_id;
		$this->settings->client_secret = $client_secret;
		$this->settings->redirect_uri = $redirect_uri;
		$this->settings->current_user_email = $current_user_email;
		$this->settings->enable = $enable;
	}
}