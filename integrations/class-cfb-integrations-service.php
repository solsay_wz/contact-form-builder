<?php

class CFB_Integrations_Service {
	
	private static $instance;

	private $integrations = [];

	private function __construct() {}

	public static function get_instance() {
		if ( empty( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function add_integration( CFB_Integration $integration ) {
		$raw_name = $integration->get_slug();
		$name = sanitize_key( $raw_name );

		if ( empty( $name ) || isset( $this->integrations[$name] ) ) {
			return false;
		}

		$this->integrations[$name] = $integration;
	}


	public function integration_exists( $name = '' ) {
		if ( '' == $name ) {
			return (bool) count( $this->integrations );
		} else {
			return isset( $this->integrations[$name] );
		}
	}

	public function get_integration( $name ) {
		if ( $this->integration_exists( $name ) ) {
			return $this->integrations[$name];
		} else {
			return false;
		}
	}

	public function get_integrations(){
		return $this->integrations;
	}

}