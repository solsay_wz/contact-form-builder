grecaptcha.ready(function() {
	recapcha_reset();
});

document.addEventListener('DOMContentLoaded', function(){

	let forms = document.querySelector('form.cfb-form');
	forms.addEventListener('reset', function(e){
		recapcha_reset();
	});

	forms.addEventListener('cfb-form-resp', function(e){
		recapcha_reset();
	});
});

function recapcha_reset(){
	grecaptcha.execute(recaptcha.site_key, {action:'validate_captcha'})
	.then(function(token) {
		document
		.querySelectorAll('[name="g-recaptcha-response"]')
		.forEach(elem => (elem.value = token));
		console.log(token);
	});
}