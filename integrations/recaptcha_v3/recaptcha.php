<?php

class Recapcha extends CFB_Integration{

	function __construct() {
		$this->settings = new CFB_Plugin_Settings( $this->get_slug() );

		if (!empty($this->settings->enable)){
			add_action('cfb_enqueue_form_scripts', [$this, 'public_scripts']);
			add_action('cfb_add_inner_html', [$this, 'add_inner_html']);
			add_action('cfb_validate_form', [$this, 'validate_form'], 10, 3);
			add_filter('custom_messages', [$this, 'add_message']);
		}


		$this->settings->asdasdasdasda = 'asdasda';

	}

	public function get_name(){
		return __('Recapcha v3', CFB_TEXT_DOMAIN);
	}

	public function get_slug(){
		return 'recaptcha';
	}

	public function add_inner_html(){
		echo '<input type="hidden" name="g-recaptcha-response">';
	}

	public function public_scripts(){
		wp_enqueue_script( 
			'recaptcha-api', 
			'https://www.google.com/recaptcha/api.js?render='.$this->get_site_key(), 
			[], 
			false, 
			true 
		);

		wp_enqueue_script( 
			'recaptcha-init', 
			CFB_PLUGIN_URI . 'integrations/recaptcha_v3/js/recaptcha-init.js', 
			['recaptcha-api'], 
			CFB_VERSION, 
			true 
		);
		wp_localize_script( 'recaptcha-init', 'recaptcha', ['site_key' => $this->get_site_key()] );

	}

	public function validate_form($error_fields, $form_data, $form_id){
		$token = $form_data['g-recaptcha-response'];
		$validate_result = $this->validate_token($token);

		if ( !$validate_result['success'] ){
			$form = new CFB_Form($form_id);
			$messages = $form->get_messages();

			$error_fields['recaptcha'] = $messages['recaptcha_spam'];
		}

		return $error_fields;
	}

	public function add_message($messages){
		$messages[] = [
			'name' => 'recaptcha_spam',
			'label' => __('Recapcha is not valid', CFB_TEXT_DOMAIN)
		];
		return $messages;
	}

	public function get_settings_fields(){
		$fields = [
			[
				'name' => 'enable',
				'label' => __('Enable?', CFB_TEXT_DOMAIN),
				'type' => 'checkbox',
				'value' => 'on'
			],
			[
				'name' => 'site_key',
				'label' => __('Site Key', CFB_TEXT_DOMAIN),
				'type' => 'text',
				'class' => 'regular-text'
			],
			[
				'name' => 'secret_key',
				'label' => __('Secret Key', CFB_TEXT_DOMAIN),
				'type' => 'text',
				'class' => 'regular-text'

			]
		];

		return $fields;
	}

	public function save_settings_form(){
		if (!$_POST) return;
		check_admin_referer( 'cfb-'.$this->get_slug().'-setup' );

		$site_key = sanitize_text_field( $_POST['site_key'] );
		$secret_key = sanitize_text_field( $_POST['secret_key'] );
		$enable = isset($_POST['enable']) ? sanitize_text_field( $_POST['enable'] ) : false;

		$this->settings->site_key = $site_key;
		$this->settings->secret_key = $secret_key;
		$this->settings->enable = $enable;
	}


	private function validate_token($token){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,'https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
			'secret' => $this->get_secret_key(), 
			'response' => $token
		]));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$json_responce = json_decode($response, true);
		return $json_responce;
	}

	private function get_site_key(){
		return $this->settings->site_key;
	}

	private function get_secret_key(){
		return $this->settings->secret_key;
	}
}