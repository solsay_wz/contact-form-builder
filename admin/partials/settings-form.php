<?php
/**
 * Global template for integration settings
 * 
 */

$page_slug = !empty( $_GET['page'] ) ? $_GET['page'] : '';
$integration_slug = str_replace('cfb-', '', $page_slug);

$integrations_service = CFB_Integrations_Service::get_instance();
$integration = $integrations_service->get_integration($integration_slug); ?>

<h1><?php echo $integration->get_name(); ?></h1>
<form method="post" action="" enctype="multipart/form-data">
	<?php wp_nonce_field( 'cfb-'.$integration->get_slug().'-setup' ); ?>
	<table class="form-table">
		<tbody>
<!-- 			<tr>
				<th scope="row"><?php //_e('Enable Integration'); ?></th>
				<td> 
					<fieldset>
						<legend class="screen-reader-text">
							<span><?php //_e('Enable Integration'); ?></span>
						</legend>
						<label for="enable">
							<input name="enable" type="checkbox" id="enable" value="1">
						</label>
					</fieldset>
				</td>
			</tr> -->
			<?php 
			$settings = $integration->get_settings();

			foreach ($integration->get_settings_fields() as $field){
                $additional_link_html = isset($field['additional_link_html']) ? $field['additional_link_html'] : '';
				$name = isset($field['name']) ? $field['name'] : false;
				$label = isset($field['label']) ? $field['label'] : false;
				$type = isset($field['type']) ? $field['type'] : 'text';
				$description = isset($field['description']) ? $field['description'] : '';

				if (!$name) continue;

                $prepare_inputs_ars = $field;
                unset($prepare_inputs_ars['additional_link_html']);
                unset($prepare_inputs_ars['description']);

                $inputs_args = join(' ', array_map(function($key) use ($prepare_inputs_ars) {
                    if(is_bool($prepare_inputs_ars[$key])) {
                        return $prepare_inputs_ars[$key]?$key:'';
                    }
                    return $key.'="'.esc_attr( $prepare_inputs_ars[$key] ).'"';
                }, array_keys($prepare_inputs_ars))); ?>
                <tr>
                    <th scope="row">
                        <label for="<?php echo $name; ?>"><?php echo $label; ?></label>
                    </th>
                    <td>
                        <?php if ($description){
                            echo $description;
                        } ?>
                        <p class="description">
                            <label for="<?php echo $name; ?>">

                                <?php if ($type === 'file') echo $settings->{$name}; ?>
                                <input
                                id="<?php echo $name; ?>"
                                <?php echo ($inputs_args);

                                if (!isset($prepare_inputs_ars['value'])){ ?>
                                    value="<?php echo $settings->{$name}; ?>"
                                <?php }

                                if ($type === 'checkbox' || $type === 'radio'){
                                    if ($settings->{$name} === $prepare_inputs_ars['value']){
                                        echo 'checked';
                                    } ?>
                                <?php } ?>
                                >
                            </label>

                            <?php echo $additional_link_html; ?>
                        </p>
                    </td>
                </tr>
			<?php } ?>
		</tbody>
	</table>
	<?php
	submit_button( __( 'Save Changes', CFB_TEXT_DOMAIN ) );
	?>
</form>