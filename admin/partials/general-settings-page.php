<?php $settings = new CFB_Plugin_Settings(); ?>
<div class="cfb-settings">
	<h1><?php _e('General Settings', CFB_TEXT_DOMAIN) ?></h1>

	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="default-to"><?php _e('Default Recipient(s)', CFB_TEXT_DOMAIN); ?></label>
				</th>
				<td>
					<p class="description">
						<label for="cfb-settings-default-to">
							<?php _e('Many emails are allowed in comma separated format', CFB_TEXT_DOMAIN); ?>
							<br>
							<input type="text" id="default-to" placeholder="example@gmail.com, example@gmai..." name="default_to" class="large-text" size="70" value="<?php echo isset($settings->general['default_to']) ? $settings->general['default_to'] : ''; ?>">
						</label>
					</p>
				</td>
			</tr>
		</tbody>
	</table>

	<div class="submit">
		<div id="cfb_save" class="button button-primary button-large"><?php _e('Save', CFB_TEXT_DOMAIN); ?></div>
	</div>

</div>