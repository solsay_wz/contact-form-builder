<?php 
global $post;
$form = new CFB_Form( $post->ID );

/**
 * custom_form_fields see @link https://formbuilder.online/docs/formBuilder/options/typeUserAttrs/
 */
$custom_fields_atts = apply_filters('cfb_custom_fields_atts', []); ?>

<div id="cfb-wrapper" class="warpper">
	<input type="hidden" name="cfb-form[form_builder_data]" id="form-builder-data" value="<?php echo esc_attr($form->get_form_builder_data()); ?>">
	<input type="hidden" name="custom_fields_atts" id="custom_fields_atts" value="<?php
	echo esc_attr(json_encode($custom_fields_atts)); ?>">
	<input class="radio" id="one" name="group" type="radio" checked>
	<input class="radio" id="two" name="group" type="radio">
	<input class="radio" id="three" name="group" type="radio">
	<input class="radio" id="four" name="group" type="radio">
	<input class="radio" id="five" name="group" type="radio">
	<div class="tabs">
		<label class="tab" id="one-tab" for="one"><?php _e('Form', CFB_TEXT_DOMAIN); ?></label>
		<label class="tab" id="mail-fields" for="two"><?php _e('Mail', CFB_TEXT_DOMAIN); ?></label>
		<label class="tab" id="three-tab" for="three"><?php _e('Messages', CFB_TEXT_DOMAIN); ?></label>
		<label class="tab" id="four-tab" for="four"><?php _e('API Logs', CFB_TEXT_DOMAIN); ?></label>
		<label class="tab" id="five-tab" for="five"><?php _e('CSS', CFB_TEXT_DOMAIN); ?></label>
	</div>

	<div class="panels">

		<div class="panel" id="one-panel">
			<div id="fb-editor"></div>
		</div>

		<div class="panel" id="two-panel">
			<div id="form-fields-name">

			</div>

			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="cfb-mail-to"><?php _e('To', CFB_TEXT_DOMAIN); ?></label>
						</th>
						<td>
							<input type="text" id="cfb-mail-to" placeholder="example@gmail.com, example@gmai..." name="cfb-form[to]" value="<?php echo esc_attr($form->get_to()); ?>">
						</td>
					</tr>

					<tr>
						<th scope="row">
							<label for="cfb-mail-from"><?php _e('From', CFB_TEXT_DOMAIN); ?></label>
						</th>
						<td>
							<input type="text" id="cfb-mail-from" name="cfb-form[from]" value="<?php echo esc_attr($form->get_from()); ?>">
						</td>
					</tr>

					<tr>
						<th scope="row">
							<label for="cfb-mail-subject"><?php _e('Subject', CFB_TEXT_DOMAIN); ?></label>
						</th>
						<td>
							<input type="text" id="cfb-mail-subject" name="cfb-form[subject]" value="<?php echo esc_attr($form->get_subject()); ?>">
						</td>
					</tr>

					<tr>
						<th scope="row">
							<label for="cfb-mail-additional-headers"><?php _e('Additional headers', CFB_TEXT_DOMAIN); ?></label>
						</th>
						<td>
							<textarea id="cfb-mail-additional-headers" rows="6" placeholder="Reply-To: example@gmail.com" name="cfb-form[additional_headers]"><?php echo $form->get_additional_headers(); ?></textarea>
						</td>
					</tr>

					<tr>
						<th scope="row">
							<label for="cfb-mail-body"><?php _e('Message body', CFB_TEXT_DOMAIN); ?></label>
						</th>
						<td>
							<textarea id="cfb-mail-body" name="cfb-form[body]" placeholder="<?php _e("New user \n\nEmail: [email] \nName: [name] ", CFB_TEXT_DOMAIN); ?>" rows="15"><?php echo $form->get_body(); ?></textarea>
						</tr>

					</tbody>
				</table>
			</div>

			<div class="panel" id="three-panel">

				<p class="description">
					<label for="cfb-message-mail-sent-ok"><?php _e('Sender\'s message was sent successfully', CFB_TEXT_DOMAIN); ?>
					<br>
					<input type="text" id="cfb-message-mail-sent-ok" name="cfb-form[messages][success]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['success']) ? $form->get_messages()['success'] : '' ; ?>">
				</label>
			</p>

			<p class="description">
				<label for="wpcf7-message-mail-sent-ng"><?php _e('Sender\'s message failed to send', CFB_TEXT_DOMAIN); ?>
				<br>
				<input type="text" id="cfb-message-mail-sent-ng" name="cfb-form[messages][error]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['error']) ? $form->get_messages()['error'] : ''; ?>">
				</label>
			</p>

			<p class="description">
				<label for="wpcf7-message-mail-sent-invalid"><?php _e('There is a field that the sender must fill in', CFB_TEXT_DOMAIN); ?>
				<br>
				<input type="text" id="cfb-message-mail-sent-invalid" name="cfb-form[messages][invalid_required]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['invalid_required']) ? $form->get_messages()['invalid_required'] : ''; ?>">
				</label>
			</p>

			<p class="description">
				<label for="wpcf7-message-mail-sent-unexpect-file"><?php _e('Uploading a file fails for any reason', CFB_TEXT_DOMAIN); ?>
				<br>
				<input type="text" id="cfb-message-mail-sent-unexpect-file" name="cfb-form[messages][unexpect_file]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['unexpect_file']) ? $form->get_messages()['unexpect_file'] : ''; ?>">
				</label>
			</p>


			<p class="description">
				<label for="wpcf7-message-mail-sent-invalid-mail"><?php _e('Email address that the sender entered is invalid', CFB_TEXT_DOMAIN); ?>
				<br>
				<input type="text" id="cfb-message-mail-sent-unexpect-invalid-mail" name="cfb-form[messages][invalid_mail]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['invalid_mail']) ? $form->get_messages()['invalid_mail'] : ''; ?>">
				</label>
			</p>

            <p class="description">
                <label for="wpcf7-message-mail-sent-invalid-mail"><?php _e('Email address not contains "@" or "."', CFB_TEXT_DOMAIN); ?>
                    <br>
                    <input type="text" id="cfb-message-mail-sent-unexpect-invalid-mail-symbol" name="cfb-form[messages][invalid_mail_symbol]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['invalid_mail_symbol']) ? $form->get_messages()['invalid_mail_symbol'] : ''; ?>">
                </label>
            </p>

            <p class="description">
                <label for="wpcf7-message-mail-sent-invalid-mail"><?php _e('Email contains no latin characters', CFB_TEXT_DOMAIN); ?>
                    <br>
                    <input type="text" id="cfb-message-mail-sent-unexpect-non-latin-char" name="cfb-form[messages][non_latin_char]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['non_latin_char']) ? $form->get_messages()['non_latin_char'] : ''; ?>">
                </label>
            </p>

			<p class="description">
				<label for="wpcf7-message-mail-sent-invalid-tel"><?php _e('Phone number that the sender entered is invalid', CFB_TEXT_DOMAIN); ?>
				<br>
				<input type="text" id="cfb-message-mail-sent-unexpect-invalid-mail" name="cfb-form[messages][invalid_tel]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['invalid_tel']) ? $form->get_messages()['invalid_tel'] : ''; ?>">
				</label>
			</p>

            <p class="description">
                <label for="wpcf7-message-mail-sent-invalid-tel"><?php _e('Phone contains non numbers chars', CFB_TEXT_DOMAIN); ?>
                    <br>
                    <input type="text" id="cfb-message-mail-sent-unexpect-invalid-phone-field" name="cfb-form[messages][non_number_char]" class="large-text" size="70" value="<?php echo isset($form->get_messages()['non_number_char']) ? $form->get_messages()['non_number_char'] : ''; ?>">
                </label>
            </p>

			<?php $custom_messages = apply_filters('custom_messages', []);
			foreach ($custom_messages as $custom_message){
				$name = $custom_message['name'];
				$label = $custom_message['label']; ?>
				<p class="description">
					<label for="message-mail-sent-<?php echo $name ?>">
						<?php echo $label; ?>
					<br>
					<input type="text" id="cfb-message-mail-sent-<?php echo $name ?>" 
					name="cfb-form[messages][<?php echo $name ?>]" class="large-text" size="70" 
					value="<?php echo isset($form->get_messages()[$name]) ? $form->get_messages()[$name] : ''; ?>">
				</label>
			</p>
			<?php } ?>



	</div>
	<div class="panel" id="four-panel">
		<?php 

		$integrations_service = CFB_Integrations_Service::get_instance();
		$integrations = $integrations_service->get_integrations();
		if ( $integrations ){ ?>

			<div class="top-selects">
				
				<div class="alignleft integration-select bulkactions">
					<select name="integration" id="bulk-action-selector-top">
						<option disabled selected><?php _e('Select integration', CFB_TEXT_DOMAIN); ?></option>
						<?php foreach( $integrations as $integration ){ ?>
							<option value="<?php echo $integration->get_slug(); ?>" class="hide-if-no-js">
								<?php echo $integration->get_name(); ?>
							</option>
						<?php } ?>

					</select>
				</div>
				<div class="alignleft perday-integration-select bulkactions">
					<label for="bulk-action-selector-top" class="screen-reader-text"><?php _e('Select Day', CFB_TEXT_DOMAIN); ?></label>
					<select name="day" id="bulk-action-selector-top">

					</select>
				</div>

			</div>

			
		<?php } ?>

		<div class="resp-container"></div>
	</div>

	<div class="panel" id="five-panel">

		<label>
			<textarea id="cfb-form[form_css]"  name="cfb-form[form_css]" type="text" data-id="admin-css"/><?php echo $form->get_form_css(); ?></textarea>
			<div id="admin_css"></div>
		</label>
	</div>

</div>
</div>
