<?php

/**
 * Metabox
 */
class Contact_Form_MetaBox {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		add_action('add_meta_boxes_cfb-form', [$this, 'add_form_metabox']);
		add_action('save_post', [$this, 'save']);

        add_action('wp_ajax_cfb_logs', [$this, 'get_logs']);
        add_action('wp_ajax_cfb_save_form', [$this, 'cfb_save_form']);
	}


	/**
	 * Register the Metaboxes
	 *
	 * @since    1.0.0
	 */
	public function add_form_metabox( $post ) {
		add_meta_box( 
			'cfb-form-meta-box', 
			'Form Content', 
			[$this, 'render_metabox']
		);
	}

	/**
	 * Save form data
	 *
	 * @since    1.0.0
	 */
	public function save( $post_id ) {
		$form = new CFB_Form($post_id);
		if (empty($_POST['cfb-form'])) return;

		$form_data = $_POST['cfb-form'];
		foreach ($form_data as $name => $value) {

			switch ($name) {

				case 'form_builder_data':
					$form->set_form_builder_data($value);
					break;

				case 'from':
					$form->set_from($value);
					break;

				case 'to':
					$form->set_to($value);
					break;
				
				case 'subject':
					$form->set_subject($value);
					break;

				case 'additional_headers':
					$form->set_additional_headers($value);
					break;

				case 'body':
					$form->set_body($value);
					break;

				case 'messages':
					$form->set_messages($value);
					break;

				case 'form_css':
					$form->set_form_css($value);
					break;
					
			}

		}
	}

	/**
	 * Get Logs
	 */
	public function get_logs(){
		$integration = !empty($_GET['integration']) ? $_GET['integration'] : false;
		$file_name = !empty($_GET['file_name']) ? $_GET['file_name'] : false;
		$post_id = $_GET['post_id'];

		if ($file_name){
			wp_send_json_success([
				'content' => file_get_contents($file_name),
			]);
		}

		$upload_dir = wp_get_upload_dir();
		$base_upload_dir = $upload_dir['basedir'];
		
		$base_log_dir = $base_upload_dir.'/cfb/';
		$integration_log_dir = $base_log_dir.$integration.'/'.$post_id;

		// $logs = scandir($integration_log_dir, SCANDIR_SORT_DESCENDING);
		$logs = glob($integration_log_dir.'/*');
		array_multisort(array_map('filemtime', $logs), SORT_NUMERIC, SORT_DESC, $logs);
		if (!$logs){
			wp_send_json_error([
				'content' => __('Logs not found')
			]);
		}

		ob_start(); ?>
			<div class="alignleft perday-integration-select loaded bulkactions">
				<label for="bulk-action-selector-top" class="screen-reader-text"><?php _e('Select Day', CFB_TEXT_DOMAIN); ?></label>
				<select name="file_name" id="bulk-action-selector-top">
					<?php foreach ($logs as $log){
						echo '<option value="'.$log.'">'.basename($log, '.log').'</option>';
					} ?>
				</select>
			</div>
		<?php 
		$select = ob_get_clean();



		wp_send_json_success([
			'content' => file_get_contents($logs[0]),
			'select' => $select
		]);
	}

	/**
	 * Render metabox
	 */
	public function render_metabox(){
		include_once( CFB_PATH . 'admin/partials/form-metabox.php');
	}

	/**
     * Save Form Html
     */
	public function cfb_save_form(){
        $html = isset( $_POST['html'] ) ? $_POST['html'] : '';
        $id = isset( $_POST['id'] ) ? $_POST['id'] : '';

        $form = new CFB_Form($id);
        $form->set_html($html);
    }
}
