/**
 * Shortcode element - dynamic render shortcode
 */

// configure the class for runtime loading
if (!window.fbControls) window.fbControls = []
window.fbControls.push(function(controlClass) {

  class controlShortcode extends controlClass {
    /**
     * Class configuration - return the icons & label related to this control
     * @returndefinition object
     */
    static get definition() {
      return {
        icon: '<span class="dashicons dashicons-shortcode"></span>',
        i18n: {
          default: 'Shortcode',
        },
      }
    }

    /**
     * javascript & css to load
     */
    configure() {
    }

    /**
     * build a text DOM element, supporting other jquery text form-control's
     * @return {Object} DOM Element to be injected into the form.
     */
    build() {
      return this.markup('div', null, { id: this.config.name }, '['+this.config.value+']')
    }

    /**
     * onRender callback
     */
    onRender() {
      const value = this.config.value || ''
      jQuery('#' + this.config.name).text(this.config.value)
    }

  }

  // register this control for the following types & text subtypes
  controlClass.register('shortcode', controlShortcode)
  return controlShortcode
})