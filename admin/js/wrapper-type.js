/**
 * Wrapper element
 */

// configure the class for runtime loading
if (!window.fbControls) window.fbControls = []
window.fbControls.push(function(controlClass) {

    class wrapperElement extends controlClass {
        /**
         * Class configuration - return the icons & label related to this control
         * @returndefinition object
         */
        static get definition() {
            return {
                icon: "<span class=\"dashicons dashicons-embed-generic\"></span>",
                className: "wrapper_type_close",
                i18n: {
                    default: 'Wrapper',
                },
            }
        }

        /**
         * javascript & css to load
         */
        configure() {
        }

        /**
         * build a text DOM element, supporting other jquery text form-control's
         * @return {Object} DOM Element to be injected into the form.
         */
        build() {
            return '<div class="'+this.rawConfig.className+'"></div>';//this.markup('div', null, { id: this.config.name, className: this.rawConfig.className }, '['+this.config.value+']')
        }

        /**
         * onRender callback
         */
        onRender() {
            jQuery('#' + this.config.name).text('<wrapper>')
        }

    }

    // register this control for the following types & text subtypes
    controlClass.register('wrapper', wrapperElement)
    return wrapperElement
})

jQuery(function($) {
    "use strict";

    document.addEventListener('fieldAdded', function(e){
        addCloseWrapperField(e);
        indexAll(e);
         // renderWrapperZone
        // ();
    });
    document.addEventListener('viewData', indexAll);
    document.addEventListener('fieldEditOpened', indexAll);
    $(document).on('mouseup', '.post-type-cfb-form ul.stage-wrap.ui-sortable > li', indexAll);

    function indexAll(){
        $('#fb-editor ul.ui-sortable.stage-wrap > li').addClass('haveIndex');
    }

    function addCloseWrapperField(e){

        $('#fb-editor ul.ui-sortable.stage-wrap > li.wrapper-field:not(.haveIndex)').each(function (i){
            let classes = $(this).find('[name="className"]').val();
            let is_close_tag = !!$(this).find('.wrapper_type_close').length;

            if (is_close_tag){
                return true;
            } else {
                if ( ! classes.includes('has_close')){
                    var field = {
                        type: 'wrapper',
                        className: 'wrapper_type_not_close',
                        label: 'Wrapper Open'
                    }

                    window['form_builder'].actions.addField(field, $(this).index());
                    $(this).find('[name="className"]').val(classes+' has_close');
                }
            }
        });

    }
});

