jQuery(function($) {
	"use strict";

	if ($('#form-builder-data').length > 0 ){
		let custom_fields_atts = $('#custom_fields_atts').val();
		let saved_data = $('#form-builder-data').val();


		var form_builder = (saved_data) ?
			$('#fb-editor').formBuilder( {formData: saved_data, dataType: 'json', typeUserAttrs: JSON.parse( custom_fields_atts )} ) :
			$('#fb-editor').formBuilder({typeUserAttrs: JSON.parse( custom_fields_atts ) });

		window['form_builder'] = form_builder;
	}

	// Render new fields on mail body settings
	$('#mail-fields').click(function(e){
		let form_fields = get_form_fields();

		$('#form-fields-name').html('');
		form_fields.forEach(function(field){
			$('#form-fields-name').append(
				'<span class="copy">['+
				field.name +
				']</span>');
		});
	});


	// Handle post save for add\update form fields
	$('#post').submit(function(e){
		$('#form-builder-data').val( JSON.stringify(get_form_fields()) );

		$('body').append( '<div id="cfb-temp" style="display: none;"></div>' );
		$('#cfb-temp').formRender({
			formData: JSON.stringify(get_form_fields()),
			layoutTemplates: {
				build : function ( data) {

					return $('<label/>')
						.attr('for', data.id)
						.append(label);

				}
			}
		});


		if ( !$('#post').hasClass('ajax_loaded') ){
			e.preventDefault();
			$('#cfb-temp [access]').removeAttr('access');
			$('#cfb-temp textarea').removeAttr('type');
			$('#cfb-temp button').removeAttr('style');
			$('#cfb-temp [required_all]').removeAttr('required_all');
			$('#cfb-temp .formbuilder-checkbox-group-label,' +
				'#cfb-temp .formbuilder-radio-group-label,' +
				'#cfb-temp .formbuilder-shortcode-label,' +
				'#cfb-temp .formbuilder-wrapper-label').removeAttr('for');

			$('#cfb-temp [selected]').attr('selected', '');
			$('#cfb-temp [multiple]').attr('multiple', '');

			let iterration = 1;
			$('#cfb-temp .formbuilder-wrapper').each(function (){
				let is_close = !$(this).find('.wrapper_type_not_close').length > 0;

				if (is_close){
					$(this).replaceWith('[cfb-wrapper]');
				}else{
					$(this).replaceWith('[cfb-wrapper id="'+iterration+'"]');
					iterration++;
				}

			});

			let form_html = $('#cfb-temp').html();
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				data: {
					action: 'cfb_save_form',
					id: $('#post_ID').val(), html: form_html
				},
				success: function (){
					$('#post').addClass('ajax_loaded');
					$('#post').submit();
				}
			});
		}

	});

	$('#cfb_save').click(function(e){
		let $btn = $(this);
		$($btn).addClass('load');

		let inputs = $btn.closest('.cfb-settings').find('input');
		let prepare_inputs = {};
		inputs.each(function(){
			prepare_inputs[$(this).attr('name')] = $(this).val();
		});

		let data = {
			action: 'cfb_settings',
			fields: prepare_inputs,
		};
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: data,
			success: function(result) {
				$btn.removeClass('load');
			}
		});

	});


	$(document).on('change', 'select[name="integration"], select[name="file_name"]', function(e){
		let $select = $(this);
		$select.closest('.panel').addClass('load');

		let data = {
			action: 'cfb_logs',
			post_id: new URLSearchParams(window.location.search).get('post')
		};

		if ($select.attr('name') === 'file_name'){
			data.file_name = $select.val();
		}else{
			data.integration = $select.val();
			data.file_name = $('select[name="file_name"]').val();

		}

		$.ajax({
			url: ajaxurl,
			type: 'GET',
			data: data,
			success: function(result) {
				$select.closest('.panel').removeClass('load');

				$select.closest('.panel').find('.resp-container').html( result.data.content );

				if (result.data.select){
					$('.perday-integration-select').replaceWith(result.data.select);
				}
			}
		});

	});


	$(document).on('click','.copy', function(e){
		let $this = $(this);
		$this.addClass('copied');
		copy_to_clipboard($this);

		setTimeout(function(){
			$($this).removeClass('copied');
		}, 200);

	});

	function copy_to_clipboard(elem) {
		let $temp = $("<input>");
		let copyText = $(elem).text();
		$("body").append($temp);
		$temp.val(copyText).select();
		document.execCommand("copy");
		$temp.remove();
	}

	function get_form_fields(){
		return form_builder.actions.getData();
	}

	var editor = ace.edit("admin_css");
	editor.setTheme("ace/theme/monokai");
	editor.getSession().setMode("ace/mode/css");
	var textarea = $('textarea[data-id="admin-css"]');
	editor.getSession().setValue(textarea.text());
	editor.getSession().on('change', function(){
		textarea.text(editor.getSession().getValue());
	});
});