<?php

/**
 * The admin-specific functionality of the plugin.
 */
class Contact_Form_Builder_Admin {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		add_action('admin_enqueue_scripts', [$this, 'enqueue_styles']);
		add_action('admin_enqueue_scripts', [$this, 'enqueue_scripts']);

		add_action('admin_menu', [$this, 'add_submenu_page']);
		add_action('wp_ajax_cfb_settings', [$this, 'settings_save']);

		add_action('edit_form_before_permalink', [$this, 'render_form_shortcode']);

        add_shortcode('cfb_close_tag', [$this, 'close_tag_shortcode']);
        add_shortcode('cfb_open_tag', [$this, 'open_tag_shortcode']);

		new Contact_Form_MetaBox();
	}

	/**
	 * Add Settings page's
	 *
	 * @since    1.0.0
	 */
	public function add_submenu_page(){
		add_submenu_page(
			'edit.php?post_type=cfb-form',
			'General Settings',
			'General Settings',
			'manage_options',
			'cfb-settings',
			[$this, 'render_settings_page']
		);

		$integrations_service = CFB_Integrations_Service::get_instance();
		$integrations = $integrations_service->get_integrations();
		if ( $integrations ){

			foreach( $integrations as $integration_slug => $integration ){
				if (!$integration->get_settings_fields()) continue;

				$integration_menu = add_submenu_page(
					'edit.php?post_type=cfb-form',
					$integration->get_name(),
					$integration->get_name(),
					'manage_options',
					'cfb-'.$integration_slug,
					[$integration, 'render_page']
				);

				add_action( 'load-' . $integration_menu, [$integration, 'save_settings_form'], 10, 0 );
			}
		}
	}

	/**
	 * Render settings page
	 *
	 * @since    1.0.0
	 */
	public function render_settings_page(){
		include_once( CFB_PATH . 'admin/partials/general-settings-page.php');
	}

	/**
	 * Save general settings
	 */
	public function settings_save(){
		if (empty($_POST['fields'])) return;

		$settings = new CFB_Plugin_Settings();
		$fields = $_POST['fields'];
		
		$settings->general = $fields;

		wp_send_json_success();
	}


	/**
	 * Render form shortcode
	 */
	public function render_form_shortcode(){
		global $post;
		if ($this->is_plugin_page()){
			echo '<span id="form_shortcode" class="copy shortcode">[contact-form id="'.$post->ID.'"]</span>';
		}

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( 'cfb-main', plugin_dir_url( __FILE__ ) . 'css/cfb-main.css', [], CFB_VERSION , 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script('jquery-ui-sortable');
		
		wp_enqueue_script( 'form-builder', plugin_dir_url( __FILE__ ) . 'js/form-builder.min.js', [ 'jquery' ], CFB_VERSION, false );
		wp_enqueue_script( 'form-render', plugin_dir_url( __FILE__ ) . 'js/form-render.min.js', [ 'jquery' ], CFB_VERSION, false );

		wp_enqueue_script( 'cfb-ace', plugin_dir_url( __FILE__ ) . 'js/ace.js', [ 'jquery-ui-sortable' ] );
		wp_enqueue_script( 'cfb-mode', plugin_dir_url( __FILE__ ) . 'js/mode-css.js', [ 'jquery-ui-sortable' ] );
		wp_enqueue_script( 'cfb-monokai', plugin_dir_url( __FILE__ ) . 'js/theme-monokai.js', [ 'jquery-ui-sortable' ] );

		wp_enqueue_script( 'cfb-main', plugin_dir_url( __FILE__ ) . 'js/cfb-main.js', [ 'jquery' ], CFB_VERSION, false );
        wp_enqueue_script( 'cfb-shortcode-field', plugin_dir_url( __FILE__ ) . 'js/shortcode-type.js', [ 'form-render', 'form-builder' ], CFB_VERSION, false );
        wp_enqueue_script( 'cfb-wrapper-field', plugin_dir_url( __FILE__ ) . 'js/wrapper-type.js', [ 'form-render', 'form-builder' ], CFB_VERSION, false );

	}

	/**
	 * If this plugin page
	 */
	public function is_plugin_page(){
		$screen = get_current_screen();

		return $screen->id === 'cfb-form';
	}


	/**
     * Open & close shortcodes for wrappers
     */
	public function close_tag_shortcode($atts){
	    return '</div>';
    }

    public function open_tag_shortcode($atts){
        return '<div '.implode(' ', $atts).'>';
    }

}
