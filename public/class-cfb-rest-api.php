<?php

class CFB_Rest_API {
    public function __construct()	{
        add_action( 'rest_api_init', [$this, 'rest_api_init'], 10, 0 );
    }

    public function rest_api_init(){
        register_rest_route( 'cfb/v1',
            '/contact-forms/(?P<id>\d+)',
            array(
                array(
                    'methods' => WP_REST_Server::CREATABLE,
                    'callback' => [$this, 'rest_create_feedback'],
                    'permission_callback' => '__return_true',
                ),
            )
        );

        register_rest_route( 'cfb/v1',
            '/contact-forms/(?P<id>\d+)/css',
            array(
                array(
                    'methods' => WP_REST_Server::READABLE,
                    'callback' => [$this, 'rest_get_form_css'],
                    'permission_callback' => '__return_true',
                ),
            )
        );
    }

    public function rest_create_feedback($request){
        $form_id = $request->get_param( 'id' );
        $form_fields = array_merge($request->get_file_params(), $request->get_params());
        $form = new CFB_Form($form_id, $form_fields);


        $errors = $form->validate_form();

        $result = '';
        if ( !$errors ){
            $result = $form->submit();
        }

        $message = '';
        $messages = $form->get_messages();
        if ($result){
            $message = $messages['success'];
        }else{
            $message = $messages['error'];
        }

        $response = [
            'errors' => $errors,
            'send' => $result,
            'message' => $message,
            'trash' => ob_get_clean()
        ];
        return rest_ensure_response( $response );
    }

    public function rest_get_form_css($request){
        $form_id = $request->get_param( 'id' );
        $form = new CFB_Form($form_id);

        header('Content-Type: text/css; charset=UTF-8');
        echo $form->get_form_css();
        die();
    }
}