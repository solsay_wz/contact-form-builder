document.addEventListener('DOMContentLoaded', function(){

	let forms = document.querySelector('form.cfb-form');
	forms.addEventListener('submit', function(e){
		e.preventDefault();

		let form = this;

		// Start event
		let before_send = new Event('cfb-form-before-send', {
			cancelable: true
		});
		let before_send_is_cancel = !form.dispatchEvent(before_send);
		if (before_send_is_cancel) {
			return;
		}

		form.classList.add('load');
		clean_errors(form);

		const XHR = new XMLHttpRequest();
		let formData = new FormData(form);

		XHR.addEventListener( 'load', function(event) {
			let response = JSON.parse(XHR.response);

			if (response.errors){
				form.classList.add('validation-failed');

				for (let field_name in response.errors) {
					let message = response.errors[field_name];
					let field = form.querySelector('[name="'+field_name+'"]');

					if (field){

						let field_wrap = field.closest('.form-group');
						field_wrap.setAttribute('data-error', message);

					}else{
						response.message += '<br>'+message;
					}
					
				}
			}else{
				form.remove.add('validation-failed');
			}

			if (response.send){
				form.reset();
				form.querySelector('.response').classList.add('success');
				form.querySelector('.response').classList.remove('error');
			}else{
				form.querySelector('.response').classList.remove('success');
				form.querySelector('.response').classList.add('error');
			}

			form.querySelector('.response').innerHTML = response.message;
			form.querySelector('.response').style.display = 'block';
			setTimeout(function (){
				form.querySelector('.response').style.display = 'none';
			}, 2000);

			let end_load = new Event('cfb-form-resp');
			form.dispatchEvent(end_load);
			form.classList.remove('load');			
		} );

		XHR.addEventListener( 'error', function(event) {
			form.classList.remove('load');			
		} );

		XHR.open( this.getAttribute('method'), this.getAttribute('action') );
		XHR.send( formData );
		return;
	});

	function clean_errors(form){
		let err_fields = form.querySelectorAll('[data-error]');
		if (err_fields){

			for (i = 0; i < err_fields.length; ++i) {
				err_fields[i].removeAttribute('data-error');
			}
		}
		// form.querySelector('.response').textContent = '';
		// form.querySelector('.response').style.display = 'none';

		// let errors = form.querySelector('span.error');
		// if (errors) errors.remove();
	}

	function reset_form(form){
		form.reset();
	}

});