document.addEventListener('DOMContentLoaded', function(){

    let forms = document.querySelectorAll('form.cfb-form');
    forms.forEach(form_item => {
        form_item.addEventListener('cfb-form-before-send', function(e){
            let form = e.target;

            let has_error = false;
            let inputs = form.querySelectorAll('[name]');

            if ( inputs.length ){
                let messages = window['form_'+form.getAttribute('data-form-id')+'_messages'];
                inputs.forEach(function (input){
                    let type = input.getAttribute('type');
                    let value = input.value;
                    let is_required = input.getAttribute('required');

                    if (is_required){
                        if (!value){
                            has_error = true;
                            show_error(input, messages.invalid_required);
                        }

                        if (type === 'checkbox' && input.checked === false){

                            if (!input.classList.contains('radio-checkbox')){
                                has_error = true;
                                show_error(input, messages.invalid_required);
                            }else{
                                let checkboxes = input.closest('.checkbox-group').querySelectorAll('input');
                                let someone_is_checked = false;
                                checkboxes.forEach(checkbox_item => {
                                    if (checkbox_item.checked) someone_is_checked = true;
                                });

                                if (! someone_is_checked){
                                    has_error = true;
                                    show_error(input, messages.invalid_required);
                                }
                            }

                        }
                    }

                    if (value){
                        if (type === 'email'){
                            const regexp_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                            if (!value.includes('@') || !value.includes('.'))  {
                                has_error = true;
                                show_error(input, messages.invalid_mail_symbol);

                            }else if (! regexp_email.test(String(value).toLowerCase())){
                                has_error = true;
                                show_error(input, messages.invalid_mail);
                            }

                        }
                    }
                });
            }

            if ( has_error ){
                form.classList.add('validation-failed');
                e.preventDefault();
            }else{
                form.classList.remove('validation-failed');
            }
        });
    });

    let clear_inputs = document.querySelectorAll('form.cfb-form [name]:not([type="checkbox"])');
    clear_inputs.forEach(clear_item => {
        clear_item.addEventListener('focus', function (e){
            let clear_input = e.target;
            let clear_input_wrap = clear_input.closest('.field-'+ clear_input.getAttribute('name'));
            if (clear_input_wrap){
                clear_input_wrap.removeAttribute('data-error');
            }
        });
    });

    let email_fields = document.querySelectorAll('form.cfb-form [type="email"]');
    email_fields.forEach(email_item => {
        email_item.addEventListener('keydown', function (e){
            let email_input = e.target;
            let email_input_wrap = email_input.closest('.field-'+ email_input.getAttribute('name'));

            email_input_wrap.removeAttribute('data-error');

            var regexp_latin = /([^\u0000-\u007f])/;
            if (regexp_latin.test(email_input.value)){
                let messages = window['form_'+email_input.closest('form').getAttribute('data-form-id')+'_messages'];
                show_error(email_input, messages.non_latin_char);
            }
        });
    });

    let phone_fields = document.querySelectorAll('form.cfb-form [type="tel"]');
    phone_fields.forEach(phone_item => {
        phone_item.addEventListener('keydown', validate_phone);
        phone_item.addEventListener('keyup', validate_phone);
    });

    let checkboxes = document.querySelectorAll('form.cfb-form [type="checkbox"]');
    checkboxes.forEach(checkbox_item => {
        checkbox_item.addEventListener('change', function (e){
            let checkbox = e.target;
            let is_checked = checkbox.checked;

            if (is_checked) {
                checkbox.closest('.formbuilder-checkbox-group').removeAttribute('data-error');
            }else if(checkbox.required){
                let messages = window['form_'+checkbox.closest('form').getAttribute('data-form-id')+'_messages'];
                checkbox.closest('.formbuilder-checkbox-group').setAttribute('data-error', messages.invalid_required);
            }
        });
    });
    function validate_phone(e){
        let phone_input = e.target;
        let phone_input_wrap = phone_input.closest('.field-'+ phone_input.getAttribute('name'));

        phone_input_wrap.removeAttribute('data-error');
        if (! /^\d+$/.test(phone_input.value)){
            let messages = window['form_'+phone_input.closest('form').getAttribute('data-form-id')+'_messages'];
            show_error(phone_input, messages.non_number_char);
        }
    }

    function show_error(input, message){
        input.closest('.form-group').setAttribute('data-error',message);
    }

});