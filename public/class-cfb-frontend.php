<?php

/**
 * FrontEnd Part
 */
class CFB_FrontEnd {
    private static $instance;

    private function __construct()	{
        add_shortcode('contact-form', [$this, 'render_form']);
        add_shortcode('cfb-wrapper', [$this, 'add_wrapper']);

        new CFB_Rest_API();
    }

    public function render_form($atts){

        if ( !isset($atts['id']) ) return '';

        $this->enqueue_style();
        $this->enqueue_scripts();

        $id = (int) $atts['id'];
        $form = new CFB_Form($id);
        $css = $form->get_form_css();

        ob_start();

        $rest_base = esc_url_raw( rest_url( 'cfb/v1' ) ).'/contact-forms/'.$id;

        if ($css){
            wp_enqueue_style('form-'.$id.'-css', $rest_base.'/css');
        } ?>

        <script type="application/javascript">
            var form_<?php echo $id; ?>_messages = <?php echo json_encode( $form->get_messages() ); ?>;
        </script>

        <form class="cfb-form"
              id="<?php echo 'cfb-form_'.$id ?>"
              data-form-id="<?php echo $id; ?>"
              method="POST"
              action="<?php echo $rest_base ?>"
              novalidate >
            <?php
            do_action('cfb_add_inner_html');
            echo do_shortcode($form->get_html());
            ?>
            <div class="response" style="display: none;"></div>
        </form>
        <?php

        do_action('cfb_enqueue_form_scripts');

        return ob_get_clean();
    }

    public function add_wrapper($atts){
        if (isset($atts['id'])){
            return '<div class="form-wrapper wrapper-'.$atts['id'].'" id="wrapper-'.$atts['id'].'">';
        }else{
            return '</div>';
        }
    }

    public function enqueue_scripts(){
        wp_enqueue_script( 'cfb-main-js', CFB_PLUGIN_URI . 'public/js/main.js', [], CFB_VERSION, true );
        wp_enqueue_script( 'cfb-validation-js', CFB_PLUGIN_URI . 'public/js/validation.js', [ 'cfb-main-js' ], CFB_VERSION, true );
    }

    public function enqueue_style(){
        wp_enqueue_style( 'cfb-main', CFB_PLUGIN_URI . 'public/css/main.css' );
        wp_enqueue_style( 'cfb-validation', CFB_PLUGIN_URI . 'public/css/validation.css' );
    }

    public static function init(){
        if (!self::$instance){
            self::$instance = new CFB_FrontEnd();
        }
    }
}